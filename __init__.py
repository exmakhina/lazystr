#!/usr/bin/env python

# SPDX-FileCopyrightText: 2023 Jérôme Carretero <cJ@zougloub.eu>
#
# SPDX-License-Identifier: MIT

# -*- coding: utf-8
# Lazy evaluation of arguments for higher-performance logging purposes

from .lazystr import *
