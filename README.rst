.. -*- coding: utf-8; indent-tabs-mode:nil; -*-

#######
LazyStr
#######

:Author: `Jérôme Carretero <jerome@exmakhina.com>`_,
         ExMakhina Inc.

.. contents:: Table of Contents
..


Introduction
############

Python logging `logger.log()` method take a first stringifiable argument and
further arguments used to format the first argument.

The formatting is only performed when the record needs to be emitted,
so when logging, it's a good practice to use a template string and
arguments.
However sometimes even the arguments are expensive.

The logging level can be checked eg. with
``if logger.isEnabledFor(logging.DEBUG):``
( see https://docs.python.org/3/howto/logging.html#optimization )
but this is heavy to read.

This library brings lazy evaluation of common logging argument types.

.. include:: __init__.py

   :start-after: def __benchmark():
   :end-before: if __name__ == "__main__":
   :code: python
   :tab-width: 2


