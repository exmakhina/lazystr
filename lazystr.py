#!/usr/bin/env python

# SPDX-FileCopyrightText: 2023 Jérôme Carretero <cJ@zougloub.eu>
#
# SPDX-License-Identifier: MIT

# -*- coding: utf-8
# Lazy evaluation of arguments for higher-performance logging purposes

import logging


logger = logging.getLogger(__name__)


class LazyStr(object):
	"""
	Lazy __str__
	"""
	def __init__(self, x):
		self.x = x
	def __str__(self):
		return self.x.__str__()


class LazyMod(object):
	"""
	Lazy x % y
	"""
	def __init__(self, x, *y):
		self.x = (x, y)

	def __str__(self):
		x, y = self.x
		if not y:
			return x
		try:
			return x % y
		except Exception as e:
			logger.exception("LazyMod failed %s %% %s: %s", x, y, e)
			return "{} % {}".format(x, y)

class LazySFormat(object):
	"""
	Lazy x.format(*y, **z)
	"""
	def __init__(self, x, *y, **z):
		self.x = (x, y, z)

	def __str__(self):
		x, y, z = self.x
		return x.format(*y, **z)

class LazyFormat(object):
	"""
	Lazy format(x, y)
	"""
	def __init__(self, x, y):
		self.x = (x, y)

	def __str__(self):
		x, y = self.x
		return format(x, y)

class LazyBHex(object):
	"""
	Lazy bytes.hex(x)
	"""
	def __init__(self, data):
		self.x = data

	def __str__(self):
		return bytes(self.x).hex()


class LazyList(object):
	def __init__(self, x, lazymember=None):
		if lazymember is None:
			lazymember = LazyStr
		self.lazymember = lazymember
		self.x = x

	def __str__(self):
		return "[{}]".format(", ".join([str(self.lazymember(x)) for x in self.x]))


class LazyFun(object):
	"""
	Lazy calling of a function
	"""
	def __init__(self, fun, *args, **kw):
		self.fun = fun
		self.args = args
		self.kw = kw

	def __str__(self):
		return self.fun(*self.args, **self.kw)

