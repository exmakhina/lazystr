#!/usr/bin/env python

# SPDX-FileCopyrightText: 2023 Jérôme Carretero <cJ@zougloub.eu>
#
# SPDX-License-Identifier: MIT

# -*- coding: utf-8
# Lazy evaluation of arguments for higher-performance logging purposes

import shlex
from typing import *


def list2cmdline(lst: List[str]) -> str:
	return " ".join(shlex.quote(x) for x in lst)


class LazyCommandLine(object):
	"""

	Useful for::

	   cmd = ["echo", "pouet"]
	   logger.info("Running %s", LazyCommandLine(cmd))
	"""
	def __init__(self, cmd):
		self.cmd = cmd

	def __str__(self):
		cmd = self.cmd
		if isinstance(cmd, str):
			pass
		elif isinstance(cmd, list):
			cmd = list2cmdline(cmd)
		else:
			raise RuntimeError(cmd)

		return cmd
