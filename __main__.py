#!/usr/bin/env python

# SPDX-FileCopyrightText: 2023 Jérôme Carretero <cJ@zougloub.eu>
#
# SPDX-License-Identifier: MIT

# -*- coding: utf-8

from .lazystr import *

def _benchmark():

	import timeit
	import inspect

	if 1:
		print("LazyBHex")
		#import textwrap
		#setup = """
		# ...
		#"""
		#setup = textwrap.dedent(setup).strip()

		setup = "import logging\n"
		setup += inspect.getsource(LazyBHex)

		for l in (1, 10, 16, 20, 50, 100, 1000, 10000):
			code_a = """
			logging.debug("pat %s" % {})
			""".format('b"{}".hex()'.format("x" * l)).strip()

			code_b = """
			logging.debug("pat %s", LazyBHex({}))
			""".format('b"{}"'.format("x" * l)).strip()

			t_a = timeit.timeit(code_a, setup=setup)
			t_b = timeit.timeit(code_b, setup=setup)

			print("{: 6d} {:.3f} {:.3f}".format(l, t_a, t_b))

	if 1:
		print("LazySFormat")
		setup = "import logging\n"
		setup += inspect.getsource(LazySFormat)

		for l in (1, 10, 16, 20, 50, 100, 1000, 10000):
			code_a = """
			logging.debug("pat %s" % "a {{}}".format({}))
			""".format('"{}"'.format("x" * l)).strip()

			code_b = """
			logging.debug("pat %s", LazySFormat("a{{}}", {}))
			""".format('"{}"'.format("x" * l)).strip()

			t_a = timeit.timeit(code_a, setup=setup)
			t_b = timeit.timeit(code_b, setup=setup)

			print("{: 6d} {:.3f} {:.3f}".format(l, t_a, t_b))


if __name__ == "__main__":
	_benchmark()
